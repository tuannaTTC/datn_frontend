import Home from "@/components/home/Home";
import SignIn from "@/components/athentication/SignIn";
import SignUp from "@/components/athentication/SignUp";
import ForgotPassword from "@/components/athentication/ForgotPassword";
import Profile from "@/components/home/Profile";
import Movie from "@/components/home/Movie";
import Authentication from "@/components/athentication/Authentication";
import AdminLayout from "@/components/admin/AdminLayout";
import HomeLayout from "@/components/home/HomeLayout";
import HomeAdmin from "@/components/admin/HomeAdmin";
import User from "@/components/admin/User";
import Comments from "@/components/admin/Comments";
import Review from "@/components/admin/Review";
import AddFilm from "@/components/admin/AddFilm";
import AllFilm from "@/components/home/AllFilm";
import ViewDetailUser from "@/components/admin/ViewDetailUser";
import MovieManager from "@/components/admin/MovieManager";

export const routes = [
    {
        path: '/Admin', name: 'Admin', component: AdminLayout,
        children: [
            {path: '', component: HomeAdmin},
            {path: 'MovieManager', name: 'MovieManager', component: MovieManager},
            {path: 'User', name: 'User', component: User},
            {path: 'Comment', name: 'Comment', component: Comments},
            {path: 'User/Detail', name: 'UserDetail', component: ViewDetailUser},
            {path: 'Review', name: 'Review', component: Review},
            {path: 'AddFilm', name: 'AddFilm', component: AddFilm}
        ], beforeEnter: (to, from, next) => {
            if (localStorage.getItem('token') === null) {
                next({name: 'SignIn'})
            } else if (localStorage.getItem('roleId') === '1') {
                next()
            } else {
                next({name: 'Home'})
            }
        }
    },
    {
        path: '/Home', component: HomeLayout,
        children: [
            {path: '', name: 'Home', component: Home},
            {path: 'Profile', name: 'Profile', component: Profile},
            {path: 'Movie/:id', name: 'Movie', component: Movie},
            {path: 'AllFilm', name: 'AllFilm', component: AllFilm},
        ], beforeEnter: (to, from, next) => {
            if (localStorage.getItem('token') === null) {
                next({name: 'SignIn'})
            } else {
                next()
            }
        }
    },
    {
        path: '/', name: 'Authentication', component: Authentication,
        children: [
            {path: '', name: 'SignIn', component: SignIn},
            {path: 'SignIn', name: 'SignIn', component: SignIn},
            {path: 'SignUp', name: 'SignUp', component: SignUp},
            {path: 'ForgotPassword', name: 'ForgotPassword', component: ForgotPassword}
        ],
    },
]
