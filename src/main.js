import Vue from 'vue'
import App from './App.vue'

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

import VueCoreVideoPlayer from 'vue-core-video-player';

Vue.use(VueCoreVideoPlayer)

const options = {
    position: "top-right",
    timeout: 4000,
    closeOnClick: true,
    pauseOnFocusLoss: true,
    pauseOnHover: true,
    draggable: true,
    draggablePercent: 0.6,
    showCloseButtonOnHover: false,
    hideProgressBar: true,
    closeButton: "button",
    icon: true,
    rtl: false
};


Vue.use(Toast, options);

import {routes} from './routes'

import VueRouter from "vue-router";

Vue.use(VueRouter)


const router = new VueRouter({
    mode: "history",
    routes
})

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')
